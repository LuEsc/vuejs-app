import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/HomeApp.vue'
import HomeClient from '../components/clients/core/GeneralClient.vue'
import HomeReportCovid from '../components/ReportCovid/core/HomeReportCovid.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/informationclient',
    name: 'HomeClient',
    component: HomeClient
  },
  // Rutas para reporte de covid
  {
    path: '/reportcovid',
    name: 'HomeReportCovid',
    component: HomeReportCovid
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
