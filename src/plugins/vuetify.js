import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: {
      light: {
        primary: '#00796B',
        secondary: '#009688',
        accent: '#f5f5f5',
        error: '#FF5252',
        info: '#B2DFDB',
        success: '#4CAF50',
        warning: '#fbc02d',
        lighten_info: "#ffffff"
      },
    },
  },
});
